randomer
========

An LD_PRELOAD library that interposes srand(3) in such a way that
rand(3) produces higher-quality (/dev/urandom-seeded) streams from
run-to-runs, and yet is fully restartable with srand(3).

