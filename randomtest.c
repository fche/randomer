#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

int
main (int argc, char *argv[])
{
  int i, count = 3;
  if (argc > 1 && strlen(argv[1])) /* non-empty arg1 */
    srand (atoi (argv[1]));
  for (i=0; i<count; i++)
    printf ("%8d ", rand());

  if (argc > 2 && strlen(argv[2])) { /* non-empty arg2 */
    srand (atoi (argv[2]));
    printf ("/ ");
    for (i=0; i<count; i++)
      printf ("%8d ", rand());
  }

  printf("\n");
    
  return 0;
}
