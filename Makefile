
all:	librandomer.so randomtest

librandomer.so:	randomer.c
	gcc -fPIC -shared -o $@ $< -ldl

randomtest: randomtest.c
	gcc -o $@ $<

.PHONY:	runtest clean

clean:
	rm -f randomtest librandomer.so

runtest: randomtest librandomer.so
	@echo plain glibc: note repeated output
	./randomtest
	./randomtest
	@echo interposed but overridden
	LD_PRELOAD=./librandomer.so RANDOMER_SEED_SALT=0 ./randomtest
	LD_PRELOAD=./librandomer.so RANDOMER_SEED_SALT=0 ./randomtest "" 1
	LD_PRELOAD=./librandomer.so RANDOMER_SEED_SALT=0 ./randomtest 1 1
	@echo default: note randomer outputs from run to run
	LD_PRELOAD=./librandomer.so ./randomtest
	LD_PRELOAD=./librandomer.so ./randomtest
	LD_PRELOAD=./librandomer.so ./randomtest
	@echo note manual reseeding recreates output stream with implicit srand
	LD_PRELOAD=./librandomer.so ./randomtest "" 1
	LD_PRELOAD=./librandomer.so ./randomtest "" 1
	@echo ... and with cargo-culted explicit seeding
	pid=$$$$; \
	./randomtest $$pid $$pid; \
	./randomtest $$pid $$pid; \
	echo; \
	LD_PRELOAD=./librandomer.so ./randomtest $$pid $$pid; \
	LD_PRELOAD=./librandomer.so ./randomtest $$pid $$pid

