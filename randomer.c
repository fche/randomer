/* This interposed srand() implementation salts the srand()/rand()
   family of functions with some startup-time entropy.  This allows
   naive rand() and naive srand() users to get higher-quality random
   numbers, but also preserving the POSIX guarantee that srand(X)
   restarts a repeatable sequence. 

   Frank Ch. Eigler <fche@elastic.org>
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <stdlib.h>


static unsigned int seed_salt;		/* permanent value */

void
srand (unsigned int seed)
{
  static void (*original_srand) (int) = NULL;
  if (!original_srand)
    original_srand = dlsym (RTLD_NEXT, "srand");
  assert (original_srand);

  (*original_srand) (seed_salt ^ seed);
}


/* Choose seed-salt source. */
#define SEED_SALT_GETENV      1
#define SEED_SALT_DEV_URANDOM 1


__attribute__ ((constructor))
     static void __interposed_init ()
{
#ifdef SEED_SALT_GETENV
  {
    const char *seed_salt_str = getenv ("RANDOMER_SEED_SALT");
    if (seed_salt_str)
      {
	seed_salt = atoi (seed_salt_str);
	goto out;
      }
  }
#endif

#ifdef SEED_SALT_DEV_URANDOM
  {
    int fd, rc;
    fd = open ("/dev/urandom", O_RDONLY);
    assert (fd >= 0);
    rc = read (fd, &seed_salt, sizeof (seed_salt));
    assert (rc == 4);
    close (fd);
    goto out;
  }
#endif

out:
  srand (1);			/* as per POSIX requirements */
}
